alias apples="echo are the best"
alias partyparrot="curl parrot.live"
tmux() {
  echo "$1"
  if [[ "$1" == "a" ]]
  then
    /usr/bin/tmux $@ -d
  else
    /usr/bin/tmux $@
  fi
}
