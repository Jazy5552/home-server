" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
set nocompatible              " be iMproved, required
filetype off                  " required vundle
set omnifunc=syntaxcomplete#Complete " Use syntax completion specific to each programming
let g:python_recommended_style=0 "Remove pep8 python formating
set tags=./tags,./TAGS,tags,TAGS "Look for these tags files

set mouse=a "Enables scrolling
set encoding=utf-8
set colorcolumn=120 "Draws a red column on the 120th column
set list
set listchars=tab:>-,extends:>,precedes:<,trail:·
set shiftround "When using >> it will shift a multiple of shiftwidth
"set softtabstop=2
set number "Show line numbers on the left
"set relativenumber "Show relative line numbers on the left
"color desert "Set my color scheme
set scrolloff=5 "Show at least 5 lines before or after the cursor
set sidescrolloff=5 "Show at least 5 characters before scrolling horizontally
"Disabled because doesn't work with ssh terminals
"set mouse=a "Makes it so selecting with mouse doesn't include numbers
set laststatus=2 "Shows the status bar on every vim window
set viminfo='100,f1
set nowrapscan
set incsearch
set showcmd
"colorscheme vim-material
"set hidden "Allow changing buffers with writes still pending WARNING

" Set tab settings (Most handled by sleuth)
set shiftwidth=0 " Number of spaces text is indented during indent operation
set tabstop=2 " How many columns a tab counts for
set softtabstop=2 " How many columns to use when you hit Tab in insert mode
"set expandtab " Convert tabs to spaces when typing
"set smartindent
set autoindent
set hidden " Allow buffer switch even with changes in current buffer
set textwidth=0
set nowrap
set wrapmargin=0
set indentexpr=
set linebreak

"augroup numbertoggle
"   autocmd!
"   autocmd BufEnter,FocusGained * set relativenumber
"   autocmd BufLeave,FocusLost * set norelativenumber
"augroup END

autocmd BufWinLeave ?*.* mkview
autocmd BufWinEnter ?*.* silent loadview

" Enable 256 colors
if &term == "tmux"
	set t_Co=256
endif

if $t_Co > 2
	syntax on " Enable syntax highlighting
endif

"netrw settings
let g:netrw_liststyle=3

"airline settings
"let g:airline_powerline_fonts = 1 "Needs python and not ssh
let g:airline_theme='deus' "Also minimalist or luna or deus
let g:airline#extensions#tabline#enabled = 1 "Show buffers when only one tab is open

nnoremap ; :

" CTags
nnoremap <C-]> g<C-]>

" Reload ez
noremap <C-\> :source ~/.vimrc<CR>

" Map buffer navigation
"nnoremap \f :bn<CR>
"nnoremap gB :bp<CR>
" Mappings to access buffers (don't use "\p" because a
" delay before pressing "p" would accidentally paste).
" \l       : list buffers
" \b \f \g : go back/forward/last-used
" \1 \2 \3 : go to buffer 1/2/3 etc
nnoremap <Leader>l :ls<CR>
nnoremap <Leader>b :bp<CR>
nnoremap <Leader>f :bn<CR>
nnoremap <Leader>g :e#<CR>
nnoremap <Leader>1 :1b<CR>
nnoremap <Leader>2 :2b<CR>
nnoremap <Leader>3 :3b<CR>
nnoremap <Leader>4 :4b<CR>
nnoremap <Leader>5 :5b<CR>
nnoremap <Leader>6 :6b<CR>
nnoremap <Leader>7 :7b<CR>
nnoremap <Leader>8 :8b<CR>
nnoremap <Leader>9 :9b<CR>
nnoremap <Leader>0 :10b<CR>

" Center stuffs
noremap G Gzz
noremap n nzz
noremap N Nzz
noremap { {zz
noremap } }zz
noremap [ [zz
noremap ] ]zz

" Browser Directory
map <C-n> :Lexplore<CR>
let g:netrw_winsize = 25

"CtrlP settings
let g:ctrlp_open_multiple_files = 'ij'

" White Space highlighting
highlight ExtraWhitespace ctermbg=Blue

"Vdebug Options
if !exists('g:vdebug_options')
   let g:vdebug_options={}
   let g:vdebug_options.port=9005
   let g:vdebug_options.timeout=60
"  let g:vdebug_options.break_on_open=0
"  let g:vdebug_options = {
"         \"port" : 9005,
"         \"timeout" : 60,
"         \"debug_file" : "/home/jlleren/debug.txt",
"         \"debug_file_level" : 2,
"         \"server" : "",
"         \"ide_key" : "",
"         \"on_close" : 'detach',
"         \"break_on_open" : 1,
"         \"ide_key" : '',
"         \"path_maps" : {},
"         \"debug_window_level" : 0,
"         \"debug_file_level" : 0,
"         \"debug_file" : "",
"         \"watch_window_style" : 'expanded',
"         \"marker_default" : '⬦',
"         \"marker_closed_tree" : '▸',
"         \"marker_open_tree" : '▾'
"         \}
endif

" Disable mouse clicking, annoying when I just want to focus the terminal
noremap <LeftMouse> <NOP>

" Toggle paste mode with F2
nnoremap <F2> :set paste!<CR>
set pastetoggle=<F2>
set showmode

" Toggle spell with F3
nnoremap <F3> :set spell!<CR>

" Toggle spell with F3
nnoremap <F4> :set number!<CR>

" Quicker scrolling
noremap <C-e> 3<C-e>
noremap <C-y> 3<C-y>

" Insert real tabs
inoremap <S-Tab> <C-V><Tab>

" Don't highlight def files
autocmd BufRead,BufNewFile *.def set filetype=txt

" Wrap text files
autocmd BufNewFile,BufRead *.txt setlocal wrap
autocmd BufNewFile,BufRead *.txt setlocal spell

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" source /usr/share/vim/vim74/macros/matchit.vim

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
"Plugin 'scrooloose/nerdtree' " Ctrl + N to open a directory tree
Plugin 'tpope/vim-surround' " Surround stuff with s and visual S
Plugin 'ctrlpvim/ctrlp.vim' " File finder
Plugin 'ajh17/VimCompletesMe'
"Plugin 'ludovicchabant/vim-gutentags'
Plugin 'jlfwong/vim-mercenary' " Mecurial Wrapper
Plugin 'vimwiki/vimwiki' " Makes writing documentation pleasant
Plugin 'tpope/vim-fugitive' " Git integration
"Plugin 'joonty/vdebug' " PHP debugging NOTE: Enable after learning normal xdebuging
Plugin 'vim-airline/vim-airline-themes' " Themes for vim-airline
Plugin 'vim-airline/vim-airline' " Status-line
Plugin 'ntpeters/vim-better-whitespace' " Highlighted trailing whitespace and :StripWWhitespace
"Plugin 'ryanoasis/vim-devicons' " Pretty icons
Plugin 'vim-scripts/functionlist.vim' " Functionlist - use Flisttoggle to use
"Plugin 'stephpy/vim-yaml' " Non-slow YAML syntax highlighting
"Plugin 'tpope/vim-dispatch' " Asynchronous dispatcher to run make in the background
Plugin 'danro/rename.vim' " Easy renaming of current file
Plugin 'mileszs/ack.vim' " Ack-Grep in Vim
Plugin 'baines/vim-colorscheme-thaumaturge'
Plugin 'flazz/vim-colorschemes'
Plugin 'vim-scripts/ColorSchemeMenuMaker'
" rsync -a ~/.vim/bundle/vim-colorscheme-thaumaturge/colors/thaumaturge.vim ~/.vim/colors/
Plugin 'xolox/vim-easytags' " ctags for openned files
Plugin 'xolox/vim-misc' " Needed for easy tags
Plugin 'lumiliet/vim-twig' " Twig syntax highlight
Plugin 'xolox/vim-session' " Vim sessions
Plugin 'JamshedVesuna/vim-markdown-preview' " Markdown previewing
Plugin 'kana/vim-submode' " vim submodes
Plugin 'tpope/vim-sleuth' " autodetect spaces vs tabs
" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
"Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" GIT REPOS on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
"Plugin 'user/L9', {'name': 'newL9'}

" Auto save session
let g:session_autosave = 'yes'
let g:session_autosave_periodic = 10
let g:session_autosave_silent = 1
let g:session_autoload = 'no'

" Asyn easytags
let g:easytags_async = 1
" Override ack-grep
let g:ackprg = "ack"

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required vundle
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

"autocmd BufReadPost,FileReadPost,BufNewFile * call system("tmux rename-window ".expand("%"))
"autocmd VimLeave * call system("tmux rename-window bash")

" Colorscheme
colorscheme thaumaturge

"Nerd tree
"let NERDTreeMapActivateNode = 'l'
"let g:NERDTreeDirArrowExpandable = '+'
"let g:NERDTreeDirArrowCollapsible = '~'
"map <C-n> :NERDTreeToggle<CR>
"autocmd StdinReadPre * let s:std_in=1

"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

"Syntastic
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

"YouCompleteMe
"let g:ycm_autoclose_preview_window_after_completion = 1

" Match overlength lines
"augroup vimrc_autocmds
"  autocmd BufEnter * highlight OverLength ctermbg=black ctermfg=white guibg=black guifg=white
"  autocmd BufEnter * match OverLength /\%120v.*/
"  autocmd VimEnter * wincmd p
"augroup END

" Submode for study
call submode#enter_with('StudyMode', 'n', '', 's', ':echo "study mode"<CR>')
call submode#leave_with('StudyMode', 'n', '', 's')
call submode#map('StudyMode', 'n', '', 'j', '<C-e>')
call submode#map('StudyMode', 'n', '', 'k', '<C-y>')
call submode#map('StudyMode', 'n', '', 'h', 'zh')
call submode#map('StudyMode', 'n', '', 'l', 'zl')
let g:submode_timeout = 0

"Handlebar files as html
au BufNewFile,BufRead,BufEnter *.handlebars set filetype=html
"Spec files as xml
au BufNewFile,BufRead,BufEnter *.spec set filetype=xml
