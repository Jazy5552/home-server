#!/bin/sh
cp vimrc ~/.vimrc
cp tmux.conf ~/.tmux.conf
cp bashrc ~/.bashrc
cp bash_aliases ~/.bash_aliases

if [ ! -d "$HOME/.vim/bundle/Vundle.vim" ]; then
	git clone https://github.com/VundleVim/Vundle.vim.git "$HOME/.vim/bundle/Vundle.vim"
fi

if [ ! -d "$HOME/.tmux/plugins/tpm" ]; then
	git clone https://github.com/tmux-plugins/tpm "$HOME/.tmux/plugins/tpm"
fi
